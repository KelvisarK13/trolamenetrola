__author__ = 'Teo'

import requests as r
import re

slovarPostajalisc = dict()
with open("postaje.txt") as f:
    for vrstica in f:
        seznamcek = vrstica.strip().split(':')
        slovarPostajalisc[seznamcek[0]] = seznamcek[1]
cudnePostaje = ["Dvor"]                    # ker je telargo smotan in potem recimo za dvor najde 1000 postaj ki imajo v imenu dvor in vsaka proga ki vozi na
                                           # na bavarcu vozi tudi na dvoru
preferiranePostaje = ["BAVARSKI DVOR"]

obračališča = ["Bežigrad", "Letališka", "Zadobrova", "Litostrojska", "Litostroj", "Ježica", "Žale", "NS Rudnik"]

vseLinije = ['1 N', '1 B',"1", '2','3','3 B', '3 N','3G','5', '5 N','6','6 B','7','7 L','8','9','11','11 B','12','13','14',
             '14 B','15','18','18 L','19 B','19 I','19 Z','20','20 Z','21','22','24','25','26','27','27 K','28','29','30']


def odreziPrazenNiz(seznam):
    indeks = len(seznam)-1
    while True:
        if seznam[indeks]=="":
            indeks-=1
        else:
            seznam = seznam[:indeks+1]          # naredi pravo rezino brez praznih nizov na koncu (z indeksom prepisuje samo enkrat namesto da vsakič prepisuje
            break
    return seznam


def potegniPostajo(imePostaje, stevilkaTrole = None):
    """

    :rtype : list
    """
    if stevilkaTrole==None:
        stevilkaTrole = ""
    elif type(stevilkaTrole)==type(3):
        stevilkaTrole = [str(stevilkaTrole)]
    elif type(stevilkaTrole)==type([]):
        stevilkaTrole = [str(x) for x in stevilkaTrole]
    else:
        stevilkaTrole = [stevilkaTrole]

    if imePostaje not in slovarPostajalisc.keys():
        return list()
    else:
        sifraPostaje = slovarPostajalisc[imePostaje]

    params = {"_VIEWSTATE":"/wEPDwULLTE2MTk2MjY4MTlkZBVx62zRY0P/EKQ2cGaGd+2sXcYM",
        "lb_stationSelect":sifraPostaje,
        "tb_stationNo":imePostaje,
        "tb_routeNo":"",                          # ni pomembno če je kakšna številka ali ne
        "b_submit":"Prikaži"}

    filter = r'<td colspan="\d*">'+imePostaje+r'\s*\W\d*\W.*?</table>'
    regularniIzraz = r'<td class="RouteCol">(.*?)\r\n'
    objekt = r.post("http://bus.talktrack.com/Default.aspx", data=params)
    filtrirano = re.findall(filter, objekt.text, re.DOTALL)
    seznam = re.findall(regularniIzraz, ''.join(filtrirano), re.DOTALL)
    trole = []

    for k in seznam:
        k=k[:-5]                                  # odreže zadnji </td>
        trola = k.split('</td><td>')
        trola = odreziPrazenNiz(trola)
        ime = trola[0].split(' ')
        if len(ime)>1 and len(ime[1])==1:
            linija = ' '.join(ime[:2])
        else:
            linija = ime[0]
        if (trola[0].split(' ')[0]=='Route Zbriši to ko nerabiš več!!' or linija in stevilkaTrole or stevilkaTrole=="") and not ('GARAŽA' in trola[0].split(' ')):                 # vzame samo želene številke, ne upošteva garaže in obdrži ločni stolpec
            trole.append(trola)

    return trole

def izgradiLinijo(stevilka, zacetnoPostajalisce = None):
    """

    :rtype : list
    """
    postajeLinije = []
    for postaja in slovarPostajalisc.keys():
        potegnjenaPostaja = potegniPostajo(postaja, stevilka)
        if len(potegnjenaPostaja)>0:
            postajeLinije.append(postaja)
    return postajeLinije

















for k in potegniPostajo("Brodarjev trg"):
    print(k)
























